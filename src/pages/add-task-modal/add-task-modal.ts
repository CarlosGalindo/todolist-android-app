import { Component } from '@angular/core';
import { ViewController, IonicPage, NavParams } from 'ionic-angular';
import { TodoModel } from '../../shared/todo-model';

/**
 * Generated class for the AddTaskModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task-modal',
  templateUrl: 'add-task-modal.html',
})
export class AddTaskModalPage {

  public model = new TodoModel('');
  public title:string = "Add new task";
  public buttonText:string = "ADD"; 

  constructor(
    public viewCtrl: ViewController, 
    public navParams: NavParams
  ) {
    //se comprueba si se ha mandado un todo para editarlo
    if(this.navParams.get('todo')){
      //clonar un todo para no afectar a los datos del model interno
      this.model =  TodoModel.clone(this.navParams.get('todo'));//this.navParams.get('todo');
      this.title = "Edit task";
      this.buttonText = "Save changes";
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTaskModalPage');
  }
  
  /**
   * Metodo que permite cerrar sin ningun problema la ventana pagina emergente de nueva tarea
   */
  dismiss(){
    this.viewCtrl.dismiss();
  }


  /**
   * Metodo que se ejecuta al dar clic en el boton 'Add'
   */
  submit(){
    //console.log(this.model);
    this.viewCtrl.dismiss(this.model);
  }
}
