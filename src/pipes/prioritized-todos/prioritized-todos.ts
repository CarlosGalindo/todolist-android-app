import { Pipe, PipeTransform, Inject } from '@angular/core';
import { TodoModel } from '../../shared/todo-model';

/**
 * Generated class for the PrioritizedTodosPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'prioritizedTodosPipe',
  
  //pure: false -- ya no es necesario, porque los metodos de modificacion ya son imputables
})
export class PrioritizedTodosPipe {
  /**
   * Takes a value and makes it lowercase.
   * 
   * //todo lo que llegue lo va a transformar, ejemplo de uso:
   * "myVar | prioritizedTodosPipe"
   */
  transform(item:TodoModel[]) {
    console.log("Priorized todos pipe");
    //para todo 'todo' en el 'item', regresa todos los que no sean impoerantes
    return item.filter(todo => !todo.isDone).sort((a,b) => (b.isImportant && !a.isImportant) ? 1:-1);
  }
}
